<%@page import="com.inst.mychatbot.ChatSessionRegistry"%>
<%@page import="org.alicebot.ab.Bot"%>
<%@page import="org.alicebot.ab.MagicStrings"%>
<%@page import="com.inst.mychatbot.AppSettings"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Chatbot</title>
</head>
<body>
<%
	String botName = request.getParameter("botName");
String action = request.getParameter("action");
botName="super";
action="chat";
String chatKey = ChatSessionRegistry.registerChatSession();
%>
<h2>Welcome to Chatbot</h2>
<!-- <div id="actions"> -->
<!-- 	<input type="radio" name="action" value="chat"/> Chat -->
<!-- 	<input type="radio" name="action" value="test"/> Chat -->
<!-- 	<input type="radio" name="action" value="ab"/> Chat -->
<!-- 	<input type="radio" name="action" value="aiml2csv"/> Chat -->
<!-- 	<input type="radio" name="action" value="abwq"/> Chat -->
<!-- </div> -->
<input type="hidden" name="chatKey" id="chatKey" value="<%=chatKey%>"/>
<table border="1">
	<tr><td style="background-color: gray;">My Chatbot</td></tr>
	<tr>
		<td style="height: 500px;width: 250px;">
			<div id="conversationsArea" style="border-top: 5px"></div>
		</td>
	</tr>
	<tr>
		<td style="height: 100px;width: 250px;">
			<div id="chatArea" style="width: 90%"><textarea id="talkStr" name="talkStr" rows="5" cols="50" ></textarea></div>
			<div id="chatArea" style="width: 10%"><input type="button" value="Send" onclick="submittalk()"/></div>
		</td>
	</tr>
</table>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$('#talkStr').val('');
function submittalk(){
	var chatKey = $('#chatKey').val();
	var talkStr = $('#talkStr').val();
	$('#talkStr').val('');
	$('#conversationsArea').append('<br/><span><b>Human</b>: '+talkStr+'</span>')
	var destUrl = 'botprocessor.jsp?chatKey='+chatKey+'&talkStr='+talkStr;
	$.ajax({url: destUrl, cache : false, success: function(result, textSt, xhr){
		
		$('#conversationsArea').append('<br/><span><b>Robot</b>: '+result+'</span>');
		
	    },
	    error : function(result, textSt, xhr){
	    	alert('some thing went wrong');
	    }
	});
}
</script>
</body>
</html>