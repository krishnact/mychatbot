<%@page import="org.alicebot.ab.Chat"%>
<%@page import="com.inst.mychatbot.ChatSessionRegistry"%>
<%
String chatKey = request.getParameter("chatKey");
String talkStr = request.getParameter("talkStr");
Chat chatSession = ChatSessionRegistry.getChat(chatKey);
String resp = chatSession.multisentenceRespond(talkStr);
while (resp.contains("&lt;")) resp = resp.replace("&lt;", "<");
while (resp.contains("&gt;")) resp = resp.replace("&gt;", ">");
out.print(resp);
%>
