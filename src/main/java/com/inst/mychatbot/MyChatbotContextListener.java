package com.inst.mychatbot;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.alicebot.ab.Graphmaster;
import org.alicebot.ab.MagicStrings;
import org.alicebot.ab.PCAIMLProcessorExtension;


public class MyChatbotContextListener implements ServletContextListener
{
	
	public void contextInitialized(ServletContextEvent conEvent) {
		
		AppSettings.getInstance().load();
		String botRootDir = AppSettings.getInstance().get(AppSettings.BOT_ROOT_DIR);
		MagicStrings.root_path = botRootDir;
		org.alicebot.ab.AIMLProcessor.extension = new PCAIMLProcessorExtension();
		Graphmaster.enableShortCuts = true;
		ChatSessionRegistry.register();
	}
	
	public void contextDestroyed(ServletContextEvent conEvent) {
		// TODO Auto-generated method stub
		
	}
}
