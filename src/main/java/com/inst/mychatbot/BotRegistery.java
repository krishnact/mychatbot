package com.inst.mychatbot;

import java.util.HashMap;
import java.util.Map;

import org.alicebot.ab.Bot;
import org.alicebot.ab.MagicStrings;

public class BotRegistery {
	
	private static final Map<String,Bot> botRegistry = new HashMap<String,Bot>();
	public static long botKeyCounter = 1000;
	
	public static String registerBot(String name, String action){
		String botKey = createNewBotKey();
		Bot bot = new Bot(name, MagicStrings.root_path, action);
		botRegistry.put(botKey, bot);
		return botKey; 
	}
	public static String registerBot(String name){
		String botKey = createNewBotKey();
		Bot bot = new Bot(name);
		botRegistry.put(botKey, bot);
		return botKey; 
	}
	
	private static String createNewBotKey(){
		String key = "BotKey:";
		synchronized(ChatSessionRegistry.class){
			botKeyCounter++;
			key+=botKeyCounter;
		}
		return key;
	}
	
	public static Bot getBot(String botKey){
		return botRegistry.get(botKey);
	}

}
