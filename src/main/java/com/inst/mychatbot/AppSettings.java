package com.inst.mychatbot;

import java.io.IOException;
import java.util.Properties;

public class AppSettings {
	
	public static final String BOT_ROOT_DIR = "bot.root.dir";
	private static Properties properties;
	public static AppSettings appSettings = new AppSettings();

	private AppSettings(){
		properties = new Properties();
		
	}
	
	public void load(){
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("app.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static AppSettings getInstance(){
		return appSettings;
	}
	
	public String get(String key){
		return (String)properties.get(key);
		
	}
}
