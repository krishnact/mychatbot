package com.inst.mychatbot;

import java.util.HashMap;
import java.util.Map;

import org.alicebot.ab.Chat;

public class ChatSessionRegistry {
	
	private static final Map<String,Chat> chatRegistry = new HashMap<String,Chat>();
	public static long chatKeyCounter = 1000;
	private static String botKey;
	public static void register(){
		botKey = BotRegistery.registerBot("super");
	}
	
	public static String registerChatSession(){
		String chatKey = createNewBotKey();
		System.out.println("botK:"+ botKey);
		Chat chat = new Chat(BotRegistery.getBot(botKey));
		chatRegistry.put(chatKey, chat);
		return chatKey; 
	}
	
	
	private static String createNewBotKey(){
		String key = "chatKey:";
		synchronized(ChatSessionRegistry.class){
			chatKeyCounter++;
			key+=chatKeyCounter;
		}
		return key;
	}
	
	public static Chat getChat(String botKey){
		return chatRegistry.get(botKey);
	}
	

	

}
